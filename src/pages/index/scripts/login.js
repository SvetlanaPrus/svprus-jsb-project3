const formLogin = document.querySelector(".form.form--login");
const btnLogin = formLogin.querySelector('[type = submit]');
const inputsLogin = formLogin.getElementsByTagName('input');  // incl.submit

export function login() {
    let users = JSON.parse(localStorage.getItem("credentuals")) || [];

    btnLogin.addEventListener('click', (event) => {
        event.preventDefault();

        let checkUserEmail = inputsLogin[0].value;
        let checkPassword = inputsLogin[1].value;

        if(users.length > 0){
            for (let i = 0; i < users.length; i++) {
                let existUserEmail = users[i].email;
                let existUserPassword = users[i].password;

                if (checkUserEmail === existUserEmail && checkPassword === existUserPassword) {
                    window.location.href = 'task-board.html';
                }
            }
        }
    });
}

login();
