// Validation: if this user has been already registered before -
import * as toastr from 'toastr';

export function isUserExist(newUserName) {   // string
    let users = JSON.parse(localStorage.getItem("credentuals")) || [];  // array
    let isExist;

    if(users.length > 0){
        for (let i = 0; i < users.length; i++) {
            let existUserName = users[i].name;

            if (newUserName === existUserName) {
                isExist = true;
                // alert('Такой пользователь уже существует.');
                toastr.error('Такой пользователь уже существует.');
            }
        }
    } else {
        isExist = false;
    }
    localStorage.setItem("credentuals", JSON.stringify(users));
    return isExist;
}
