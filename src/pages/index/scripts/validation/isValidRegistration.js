const Joi = require('joi');

export function isValidRegistration(obj) {
    const schema = Joi.object({
        email: Joi.string()
            .email({minDomainSegments: 2, tlds: {allow: ['com', 'net']}}),

        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{8,30}$')),

        name: Joi.string()
            .alphanum()
            .min(3)
            .max(30)
            .required()
    });

    schema.validate(obj);

    // const result = await Joi.validate(obj, schema);

    // let myAsyncFunction = async (obj, schema) => {
    //     const promise = await Joi.validate(obj, schema);
    // };

    // const promise = await Joi.validate(obj, schema).then(value => {
    //     // value -> { "a" : 123 }
    // });



    // try {
    //     await schema.validateAsync(obj);
    // } catch (err) {
    //     logMyErrors(err);
    // }

    // await myAsyncFunction();
}

