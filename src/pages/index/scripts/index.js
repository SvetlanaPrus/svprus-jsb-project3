import '../../../css/style.scss';
import {registration} from "./registration";

const divOld = document.querySelector(".form.form--registration");
const divNext = document.querySelector(".form.form--login");
const btnLogin = divOld.querySelector('.additional__info');
const btnRegistration = divNext.querySelector('.additional__info');

btnLogin.addEventListener('click', () => {    // jump to 'Login' page
    divNext.style.display = "inline";
    divOld.parentNode.replaceChild(divNext, divOld);
});

btnRegistration.addEventListener('click', () => {  // jump to 'Registration' page
    divOld.style.display = "inline";
    divNext.parentNode.replaceChild(divOld, divNext);
});














