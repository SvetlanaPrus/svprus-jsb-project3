
export function clearTeamMark(){
    const formRegistration = document.querySelector(".form.form--registration");
    const inputs = formRegistration.querySelectorAll('input');

    for(let i = 0; i < inputs.length; i++){
        if(inputs[i].getAttribute('type') !== 'submit'){
            inputs[i].setAttribute('checked', 'false');
        }
    }
}
