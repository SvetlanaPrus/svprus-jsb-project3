import {isUserExist} from "./validation/isUserExist";
import {clearTeamMark} from "./helpers/clearTeamMark";
import {login} from "./login";
import {isValidRegistration} from "./validation/isValidRegistration";

const formRegistration = document.querySelector(".form.form--registration");
const formLogin = document.querySelector(".form.form--login");
const form = formRegistration.querySelector('form');
const formSetup = document.querySelector(".form.form--setup");
const buttonRegistration = formRegistration.querySelector('[type = submit]');

const inputEmail = formRegistration.querySelector('#email');
const inputPassword = formRegistration.querySelector('#pass');
const inputName = formRegistration.querySelector('#user');

const labelsFormSetup = formSetup.querySelectorAll('.radiobtn__immitation');
const btnSubmit = formSetup.querySelector('[type = submit]');

function registration() {
    let users = JSON.parse(localStorage.getItem("credentuals")) || [];
    let user = {};

    buttonRegistration.addEventListener('click', (event) => {
        event.preventDefault();

        user.email = inputEmail.value;
        user.password = inputPassword.value;

        let isExist = isUserExist(inputName.value);

        if (isExist) {
            user = {};
            form.reset();
        } else {
            user.name = inputName.value;
            formSetup.style.display = "inline";
            formRegistration.parentNode.replaceChild(formSetup, formRegistration);
        }

        isValidRegistration(user).then(r => console.log(r));
    });

    for (let i = 0; i < labelsFormSetup.length; i++) {
        clearTeamMark();
        labelsFormSetup[i].addEventListener('click', (event) => {
            event.preventDefault();

            labelsFormSetup[i].getElementsByTagName('input')[0].checked = true;
            user.team = labelsFormSetup[i].getElementsByTagName('input')[0].id;
        });
    }

    btnSubmit.addEventListener('click', () => {
        users.push(user);
        localStorage.setItem("credentuals", JSON.stringify(users));

        formLogin.style.display = "inline";
        formSetup.parentNode.replaceChild(formLogin, formSetup);
        login();
    });
}

registration();

