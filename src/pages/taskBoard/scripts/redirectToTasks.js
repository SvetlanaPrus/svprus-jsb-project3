const ul = document.querySelector('.tab__navigation');  // ul
const tagLi = ul.getElementsByTagName('li');  // li
const tagA = ul.getElementsByTagName('a');  // a

function clearClassActive() {
    for (let s = 0; s < tagLi.length; s++) {
        tagLi[s].removeAttribute('class');
        tagA[s].setAttribute('href', '');
    }
}

function redirectToTasks() {
    tagA[0].addEventListener('click', () => {
        clearClassActive();
        tagLi[0].setAttribute('class', 'active');

        window.location.href = 'task-board.html';
    });
}

redirectToTasks();
