const ul = document.querySelector('.tab__navigation');  // ul
const tagLi = ul.getElementsByTagName('li');  // li
const tagA = ul.getElementsByTagName('a');  // a

function redirectToKanban() {

    function clearClassActive() {
        for (let s = 0; s < tagLi.length; s++) {
            tagLi[s].removeAttribute('class');
            tagA[s].setAttribute('href', '');
        }
    }

    tagA[1].addEventListener('click', () => {
        clearClassActive();
        tagLi[1].setAttribute('class', 'active');

        window.location.href = 'task-description.html';
    });
}

redirectToKanban();
