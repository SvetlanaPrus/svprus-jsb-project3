const ul = document.querySelector('.tab__navigation');  // ul
const tagLi = ul.getElementsByTagName('li');  // li
const tagA = ul.getElementsByTagName('a');  // a

function redirectToCalendar() {

    function clearClassActive() {
        for (let s = 0; s < tagLi.length; s++) {
            tagLi[s].removeAttribute('class');
            tagA[s].setAttribute('href', '');
        }
    }

    tagA[2].addEventListener('click', () => {

        clearClassActive();
        console.log(tagLi);
        console.log(tagA);

        tagLi[2].setAttribute('class', 'active');
        window.location.href = 'calendar.html';
    });
}

redirectToCalendar();
