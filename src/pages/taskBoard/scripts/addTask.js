import {newTaskOnBoard} from "./elements/newTaskOnBoard";

const mainElem = document.querySelector('.flex.kanban__block');
const columns = mainElem.querySelectorAll('.column.task__column'); // div

const formTask = document.querySelector('.form.form--task');
const taskTitle = formTask.querySelector('#taskTitle');
const taskExecutor = formTask.querySelector('#executor');
const taskDeadline = formTask.querySelector('#deadline');
const taskLabel = formTask.querySelector('#label');
const taskDescription = formTask.querySelector('#taskDescription');
let task = {};

export function hideColumns(){
    mainElem.setAttribute('class', 'task__setup');
    const columns1 = mainElem.querySelectorAll('.column.task__column');

    for(let i = 0; i < columns1.length; i++){
        columns1[i].setAttribute('style', 'display: none;');
    }
}

function openColumns(){
    mainElem.setAttribute('class', 'flex kanban__block');
    const columns2 = mainElem.querySelectorAll('.column.task__column');

    for(let i = 0; i < columns2.length; i++){
        columns2[i].setAttribute('style', 'display: inline;');
    }
}

function clearFormTask(){
    const formTask = document.querySelector('.form.form--task form');
    formTask.reset();
}

function addTask(){
    for(let i = 0; i < columns.length; i++){
        const btnAddTask = columns[i].querySelector('.item__header');

        /** Posmotret verstku knopki "Add Task". Pochemu nadpis na 2 stroki? */

        /** Dobavljaet zadanie, no tak ze dobavljaet kopii v dr.kolonki. Peresmotret! */

        btnAddTask.addEventListener('click', ()=>{                   // Button "Add Task"
            clearFormTask();
            hideColumns();
            formTask.removeAttribute('style');                       // Form for NEW TASK appear

            const btnSubmit = formTask.querySelector('[type = submit]');

            btnSubmit.addEventListener('click', (event)=>{    // Button "Submit"
                event.preventDefault();

                task.name = taskTitle.value;
                task.executor = taskExecutor.value;
                task.deadline = taskDeadline.value;
                task.label = taskLabel.value;
                task.description = taskDescription.value;

                console.log('task: ', task);

                let newElement = document.createElement('a');
                newElement.setAttribute('class', 'task task__block');
                newElement.setAttribute('href', '#');
                newElement.innerHTML = newTaskOnBoard(task);

                columns[i].insertBefore(newElement, columns[i].childNodes[2]);

                openColumns();
                formTask.setAttribute('style', 'display: none;');
            });
        });
    }
}

addTask();
