/** PROJECT: task 3 - nr.2
*  Since we don't have anything reg.task in Local Storage, we will create array of comments during the process
 *  for particular task */

export function addComment() {
    const arrayCards = document.querySelectorAll('.task__desctiption__card'); // div
    let existingComments1 = JSON.parse(localStorage.getItem("comments-TaskId-1")) || [];
    let existingComments2 = JSON.parse(localStorage.getItem("comments-TaskId-2")) || [];
    let existingComments3 = JSON.parse(localStorage.getItem("comments-TaskId-3")) || [];
    let existingComments4 = JSON.parse(localStorage.getItem("comments-TaskId-4")) || [];
    let existingComments = [existingComments1, existingComments2, existingComments3, existingComments4];
    let newComment = {};

    for (let i = 0; i < arrayCards.length; i++) {

        const input = arrayCards[i].getElementsByClassName('comment__input')[0];  // one element "input" - text
        const submit = arrayCards[i].getElementsByClassName('comment__submit')[0];  // one element "input" - submit
        const history = arrayCards[i].getElementsByClassName('comment__history')[0];  // one element "div" - history

        submit.addEventListener('click', (event) => {
            event.preventDefault();

            // Create "newComment" & send to Local Storage -
            // newComment.idTask = arrayCards[i].getAttribute('id').slice(-1); // - previous working version; OK
            let cardId = arrayCards[i].getAttribute('id').slice(-1);  // 1-4
            newComment.comment = input.value;
            let targetArray = existingComments[cardId-1];

            targetArray.push(newComment);

            localStorage.setItem("comments-TaskId-1", JSON.stringify(existingComments1));
            localStorage.setItem("comments-TaskId-2", JSON.stringify(existingComments2));
            localStorage.setItem("comments-TaskId-3", JSON.stringify(existingComments3));
            localStorage.setItem("comments-TaskId-4", JSON.stringify(existingComments4));

            // Create new element in document -
            const newElement = document.createElement('div');
            newElement.setAttribute('class', 'commet comment__block');

            newElement.innerHTML = `
                <div class="commet__icon">
                    <img src="img/userpic-big.jpg" alt="">
                </div>
                <div class="commet__text__wrapp">
                    <p class="commet__name">Svetlana Prus,<span class="commet__role">дизайнер</span></p>
                    <p class="commet__text">${input.value}</p>
                    <p class="commet__date">Сегодня, 09:00</p>
                </div>
            `;

            // Add new element to DOM -
            const arrayComments = history.querySelectorAll('.comment__block');
            history.insertBefore(newElement, arrayComments[0]);

            // Reset -
            newComment = {};
            input.value = '';
        });
    }
}

addComment();



