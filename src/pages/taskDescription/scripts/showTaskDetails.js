/** PROJECT: task 3 - nr.1 */

export function showTaskDetails() {
    const arrayOfElemA = document.querySelectorAll('.task.task__block');  // a
    const arrayCards = document.querySelectorAll('.task__desctiption__card');  // div

// Function which helps to hide card -
    function hideCard() {
        for (let k = 0; k < arrayCards.length; k++) {
            let isHidden = arrayCards[k].hidden;
            if (!isHidden) {
                arrayCards[k].hidden = true;
            }
        }
    }

// Getting the necessary card by Id of element "a" -
    function findCard(id) {
        let targetCard;
        for (let c = 0; c < arrayCards.length; c++) {
            let cardId = arrayCards[c].getAttribute('id').slice(-1);
            if (cardId === id) {
                targetCard = arrayCards[c];
            }
        }
        return targetCard;
    }

// The main function: hide prev.card & show necessary/current card -
    for (let i = 0; i < arrayOfElemA.length; i++) {
        arrayOfElemA[i].addEventListener('click', (event) => {
            event.preventDefault();

            let idFromElementA = arrayOfElemA[i].getAttribute('id').slice(-1);
            let card = findCard(idFromElementA);
            hideCard();
            card.hidden = false;
        });
    }
}

showTaskDetails();

